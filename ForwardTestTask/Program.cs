﻿using System;
using System.Collections.Generic;
using EngineSimulation.Engines;
using EngineSimulation.Engines.ICEngines;
using EngineTests;
using EngineTests.Tests;

namespace ForwardTestTask
{
    internal class Program
    {
        private static ITestBenchesModel m_TestBenchesModel;
        private static IEngine m_ICEngine;
        private static IReadOnlyList<TestBenchInfo> m_TestBenchInfos;

        private static void Main(string[] args)
        {
            Initialize();
            Console.WriteLine("Forward Test Task - Тестирование работы двигателя");
            Console.WriteLine();
            while (true)
            {
                const double defaultAmbientTemperature = AppConstants.DefaultAmbientTemperature;
                const double defaultSimulationStepLenghtInSeconds = AppConstants.DefaultSimulationStepLenghtInSeconds;
                Console.Write($"Введите температуру окружающей среды(Enter для стандартного значения {defaultAmbientTemperature}°C):");
                var ambientTemperature = ReadDouble(defaultAmbientTemperature);
                Console.Write($"Введите длину шага симуляции в секундах(Enter для стандартного значения {defaultSimulationStepLenghtInSeconds}s):");
                var simulationStepLenghtInSeconds = ReadDouble(defaultSimulationStepLenghtInSeconds);
                m_ICEngine.Configure(new EngineSimulationConfig()
                {
                    AmbientTemperature = ambientTemperature,
                    SimulationStepLenghtInSeconds = simulationStepLenghtInSeconds,
                });
                
                Console.WriteLine();
                ShowEngineTestInfos();
                Console.WriteLine();
                Console.Write("Введите номер теста двигателя:");
                var testBenchInfoIndex = ReadTestBenchInfoIndex();
                var testBenchResult = m_TestBenchesModel.DoTest(m_TestBenchInfos[testBenchInfoIndex].Key);
                Console.WriteLine();
                ShowTestResult(testBenchResult);
                Console.ReadLine();
            }
        }

        private static void ShowEngineTestInfos()
        {
            Console.WriteLine("Доступные тесты двигателя:");
            for (var i = 0; i < m_TestBenchInfos.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {m_TestBenchInfos[i].Description}");
            }
        }

        private static void Initialize()
        {
            m_ICEngine = new ICEngine();
            m_TestBenchesModel = new TestBenchesModel(m_ICEngine, AppConstants.MaxTestDurationInSeconds);
            m_TestBenchInfos = m_TestBenchesModel.GetTestBenchInfos();
        }

        private static double ReadDouble(double defaultValue)
        {
            var readValue = Console.ReadLine();
            return double.TryParse(readValue, out var result) ? result : defaultValue;
        }

        private static int ReadTestBenchInfoIndex()
        {
            while (true)
            {
                var readValue = Console.ReadLine();
                if (int.TryParse(readValue, out var result) && result >= 1 && result < m_TestBenchInfos.Count + 1)
                {
                    return --result;
                }

                Console.Write("Неверный ввод. Ещё раз введите номер теста двигателя:");
            }
        }

        private static void ShowTestResult(TestBenchResult testBenchResult)
        {
            Console.WriteLine("Результат теста:");
            Console.WriteLine($"{testBenchResult.Result}");
        }
    }
}