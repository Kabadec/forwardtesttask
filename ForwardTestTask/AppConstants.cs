﻿namespace ForwardTestTask
{
    internal static class AppConstants
    {
        public const double DefaultAmbientTemperature = 22d;
        public const double DefaultSimulationStepLenghtInSeconds = 0.1d;
        public const double MaxTestDurationInSeconds = 600d;
    }
}