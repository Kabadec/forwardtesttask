﻿using System;
using System.Collections.Generic;
using System.Linq;
using EngineSimulation.Engines;
using EngineTests.Tests;

namespace EngineTests
{
    public class TestBenchesModel : ITestBenchesModel
    {
        private readonly Dictionary<string, ITest> m_Tests;

        public TestBenchesModel(IEngine engine, double maxTestDuration)
        {
            var overheatingTest = new OverheatingTest(engine, maxTestDuration);
            var maximumPowerTest = new MaximumPowerTestBench(engine, maxTestDuration);
            m_Tests = new Dictionary<string, ITest>()
            {
                {overheatingTest.Key, overheatingTest},
                {maximumPowerTest.Key, maximumPowerTest}
            };
        }

        public TestBenchResult DoTest(string key)
        {
            if (!m_Tests.TryGetValue(key, out var test))
            {
                throw new Exception($"Тест с ключём '{key}' не существует.");
            }

            return test.DoTest();
        }

        public IReadOnlyList<TestBenchInfo> GetTestBenchInfos()
        {
            return m_Tests.Values.Select(test => test.GetInfo()).ToList();
        }
    }
}