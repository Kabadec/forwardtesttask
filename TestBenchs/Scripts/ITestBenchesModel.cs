﻿using System.Collections.Generic;
using EngineTests.Tests;

namespace EngineTests
{
    public interface ITestBenchesModel
    {
        TestBenchResult DoTest(string key);
        IReadOnlyList<TestBenchInfo> GetTestBenchInfos();
    }
}