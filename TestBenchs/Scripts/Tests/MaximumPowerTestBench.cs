﻿using System;
using EngineSimulation;
using EngineSimulation.Engines;

namespace EngineTests.Tests
{
    internal class MaximumPowerTestBench : ITest
    {
        private const string m_Key = nameof(MaximumPowerTestBench);
        private const string m_Description = "Тестовый стенд максимальной мощности.";
        private const double m_AccelerationError = 0.0001d;

        private readonly IEngine m_Engine;
        private readonly double m_MaxTestDuration;

        public string Key => m_Key;

        public MaximumPowerTestBench(IEngine engine, double maxTestDuration)
        {
            m_Engine = engine;
            m_MaxTestDuration = maxTestDuration;
        }

        public TestBenchInfo GetInfo()
        {
            return new TestBenchInfo(m_Key, m_Description);
        }

        public TestBenchResult DoTest()
        {
            m_Engine.Restart();
            var maxRecordedPower = 0d;
            var velocityCrankshaftRotation = 0d;
            IEngineSimulationStep currentEngineSimulationStep;
            do
            {
                currentEngineSimulationStep = m_Engine.NextStep();
                if (!(currentEngineSimulationStep.Power > maxRecordedPower))
                {
                    continue;
                }
                
                maxRecordedPower = currentEngineSimulationStep.Power;
                velocityCrankshaftRotation = currentEngineSimulationStep.VelocityCrankshaftRotation;

            } while (currentEngineSimulationStep.AccelerationCrankshaftRotation > m_AccelerationError &&
                     currentEngineSimulationStep.ElapsedTime <= m_MaxTestDuration);
            
            return new TestBenchResult(
                m_Key,
                $"Максимально достигнутая мощность двигателя: {Math.Round(maxRecordedPower, 2)}кВт\nПри скорости вращения коленвала: {Math.Round(velocityCrankshaftRotation, 2)}радиан/сек");
        }
    }
}