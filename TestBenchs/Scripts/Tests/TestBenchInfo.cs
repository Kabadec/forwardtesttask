﻿namespace EngineTests.Tests
{
    public class TestBenchInfo
    {
        public string Key { get; }
        public string Description { get; }

        public TestBenchInfo(string key, string description)
        {
            Key = key;
            Description = description;
        }
    }
}