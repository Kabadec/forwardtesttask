﻿using System;
using EngineSimulation;
using EngineSimulation.Engines;

namespace EngineTests.Tests
{
    internal class OverheatingTest : ITest
    {
        private const string m_Key = nameof(OverheatingTest);
        private const string m_Description = "Тестовый стенд нагрева двигателя.";

        private readonly IEngine m_Engine;
        private readonly double m_MaxTestDuration;

        public string Key => m_Key;

        public OverheatingTest(IEngine engine, double maxTestDuration)
        {
            m_Engine = engine;
            m_MaxTestDuration = maxTestDuration;
        }

        public TestBenchInfo GetInfo()
        {
            return new TestBenchInfo(m_Key, m_Description);
        }

        public TestBenchResult DoTest()
        {
            m_Engine.Restart();
            var engineImmutableData = m_Engine.GetImmutableData();
            IEngineSimulationStep currentEngineSimulationStep;
            do
            {
                currentEngineSimulationStep = m_Engine.NextStep();
                if (currentEngineSimulationStep.Temperature <= engineImmutableData.OverheatingTemperature)
                {
                    continue;
                }
                
                return new TestBenchResult(
                    m_Key,
                    $"Время, прошедшее с момента старта до перегрева: {Math.Round(currentEngineSimulationStep.ElapsedTime, 2)}s");
                
            } while (currentEngineSimulationStep.ElapsedTime <= m_MaxTestDuration);

            return new TestBenchResult(
                m_Key,
                $"Превышено максимальное время симуляции.");
        }
    }
}