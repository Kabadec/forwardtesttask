﻿namespace EngineTests.Tests
{
    internal interface ITest
    {
        string Key { get; }
        TestBenchInfo GetInfo();
        TestBenchResult DoTest();
    }
}