﻿namespace EngineTests.Tests
{
    public class TestBenchResult
    {
        public string Key { get; }
        public string Result { get; }

        public TestBenchResult(string key, string result)
        {
            Key = key;
            Result = result;
        }
    }
}