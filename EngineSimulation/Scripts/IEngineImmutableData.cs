﻿namespace EngineSimulation
{
    public interface IEngineImmutableData
    {
        public double OverheatingTemperature { get; }
        public double InertiaMoment { get; }
    }
}