﻿namespace EngineSimulation
{
    internal class EngineSimulationStep : IEngineSimulationStep
    {
        public double ElapsedTime { get; set; }
        public double Temperature { get; set; }
        public double Power { get; set; }
        public double Torque { get; set; }
        public double VelocityCrankshaftRotation { get; set; }
        public double AccelerationCrankshaftRotation { get; set; }
    }
}