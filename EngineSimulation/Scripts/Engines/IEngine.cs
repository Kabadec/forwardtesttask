﻿namespace EngineSimulation.Engines
{
    public interface IEngine
    {
        void Configure(EngineSimulationConfig engineSimulationConfig);
        void Restart();
        IEngineImmutableData GetImmutableData();
        IEngineSimulationStep NextStep();
    }
}