﻿namespace EngineSimulation.Engines.ICEngines
{
    public class ICEngine : IEngine
    {
        private readonly ICEngineImmutableData m_ImmutableData = new ICEngineImmutableData();

        private double m_AmbientTemperature;
        private double m_SimulationStepLenghtInSeconds;
        private double m_CurrentVelocityCrankshaftRotation;
        private double m_CurrentTemperature;
        private double m_ElapsedTime;
        
        public void Configure(EngineSimulationConfig engineSimulationConfig)
        {
            m_AmbientTemperature = engineSimulationConfig.AmbientTemperature;
            m_SimulationStepLenghtInSeconds = engineSimulationConfig.SimulationStepLenghtInSeconds;
        }

        public void Restart()
        {
            m_CurrentVelocityCrankshaftRotation = 0;
            m_CurrentTemperature = m_AmbientTemperature;
            m_ElapsedTime = 0;
        }

        public IEngineImmutableData GetImmutableData()
        {
            return m_ImmutableData;
        }

        public IEngineSimulationStep NextStep()
        {
            m_ElapsedTime += m_SimulationStepLenghtInSeconds;
            var torque = m_ImmutableData.GetTorque(m_CurrentVelocityCrankshaftRotation);
            var acceleration = GetAcceleration(torque);
            m_CurrentVelocityCrankshaftRotation += acceleration * m_SimulationStepLenghtInSeconds;
            var power = GetPower(torque, m_CurrentVelocityCrankshaftRotation);
            
            var heatingVelocity = GetHeatingVelocity(torque, m_CurrentVelocityCrankshaftRotation);
            var coolingVelocity = GetCoolingVelocity(m_CurrentTemperature, m_AmbientTemperature);
            var temperatureVelocity = heatingVelocity + coolingVelocity;
            m_CurrentTemperature += temperatureVelocity * m_SimulationStepLenghtInSeconds;

            return new EngineSimulationStep()
            {
                ElapsedTime = m_ElapsedTime,
                Temperature = m_CurrentTemperature,
                Power = power,
                Torque = torque,
                VelocityCrankshaftRotation = m_CurrentVelocityCrankshaftRotation,
                AccelerationCrankshaftRotation = acceleration
            };
        }
        
        private double GetPower(double torque, double velocity)
        {
            return torque * velocity / 1000;
        }

        private double GetHeatingVelocity(double torque, double velocity)
        {
            return torque * m_ImmutableData.Hm + velocity * velocity * m_ImmutableData.Hv;
        }

        private double GetCoolingVelocity(double engineTemperature, double ambientTemperature)
        {
            return m_ImmutableData.C * (ambientTemperature - engineTemperature);
        }

        private double GetAcceleration(double torque)
        {
            return torque / m_ImmutableData.InertiaMoment;
        }
    }
}