﻿namespace EngineSimulation.Engines.ICEngines
{
    internal class ICEngineImmutableData : IEngineImmutableData
    {
        private readonly double[] m_Torques = new[] { 20d, 75d, 100d, 105d, 75d, 0d };
        private readonly double[] m_VelocitiesCrankshaftRotation = new[] { 0d, 75d, 150d, 200d, 250d, 300d };
        
        public double OverheatingTemperature => 110d;
        public double InertiaMoment => 10d;
        public double Hm => 0.01d;
        public double Hv => 0.0001d;
        public double C => 0.01d;
        
        public double GetTorque(double velocityCrankshaftRotation)
        {
            if (velocityCrankshaftRotation <= m_VelocitiesCrankshaftRotation[0])
            {
                return m_Torques[0];
            }
            
            if (velocityCrankshaftRotation >= m_VelocitiesCrankshaftRotation[^1])
            {
                return m_Torques[^1];
            }

            for (var i = 0; i < m_VelocitiesCrankshaftRotation.Length - 1; i++)
            {
                var leftIndex = i;
                var rightIndex = i + 1;
                var x1 = m_VelocitiesCrankshaftRotation[leftIndex];
                var x2 = m_VelocitiesCrankshaftRotation[rightIndex];
                if (x1 <= velocityCrankshaftRotation &&
                    x2 >= velocityCrankshaftRotation)
                {
                    var y1 = m_Torques[leftIndex];
                    var y2 = m_Torques[rightIndex];
                    var y = (y2 - y1) * (velocityCrankshaftRotation - x1) / (x2 - x1) + y1;
                    return y;
                }
            }

            return default;
        }
    }
}