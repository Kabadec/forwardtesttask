﻿namespace EngineSimulation
{
    public interface IEngineSimulationStep
    {
        double ElapsedTime { get; }
        double Temperature { get; }
        double Power { get; }
        double Torque { get; }
        double VelocityCrankshaftRotation { get; }
        double AccelerationCrankshaftRotation { get; }
    }
}